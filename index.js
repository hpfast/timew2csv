const readline = require('readline');
const moment = require('moment');
const durfm = require('moment-duration-format');

durfm(moment);

const rl = readline.createInterface({
  input: process.stdin
});
process.stdin.setEncoding('utf8');


let sum = 0;
rl.on('line', (input) => {
  if (input === '[') {
    process.stdout.write('"start","end","duration","description"\n');
  } else if (input === ']') {
    return
  } else {
    let trim = input.replace(/(,$)/g, "")
    let json = JSON.parse(trim);
    let start = moment(json.start);
    let end = moment(json.end);
    let duration = moment.duration(end.diff(start)).asHours().toPrecision(3);
    let output = `"${json.start}","${json.end}","${duration}","${json.tags.join(' ')}"`
    process.stdout.write(output);
    process.stdout.write('\n');
    sum += Number(duration);
  }

})
rl.on('close', () => process.stdout.write(`"","","${sum}","TOTAL"`));

