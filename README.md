Timew2csv
---------

Create csvs from Timewarrior.

usage:

```
timew export [period] [tags] | node index.js [ > outputfile.csv]
```
